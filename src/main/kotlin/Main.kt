import java.lang.Exception

private const val MAX_ROLLS = 3

/**
 *  Main function that starts the game of Jamb.
 */
fun main(args: Array<String>) {
    val player1 = Player()
    val player2 = Player()
    var i =0
    while(i++<2){
        println("**********************************************************")
        println("*                       Player One                       *")
        println("**********************************************************")
        play(player1)
        println("\n\n")
        println("**********************************************************")
        println("*                       Player Two                       *")
        println("**********************************************************")
        play(player2)
        println("\n\n")
    }
}

/**
 *  Function that leads player through game of Jamb
 */
fun play(player: Player){
    var numLocked = 0
    var read: String?
    for( roundNumber in 0 until MAX_ROLLS){
        println("\nRoll number ${roundNumber+1}...")
        for( dicePosition in 0 until player.getNumberOfDicesAndThrows()){
            val currentDice = player.getDice(dicePosition)
            if(currentDice.isDisabled()) {
                println("Dice: ${dicePosition+1}. Dice disabled. Number rolled: ${currentDice.getCurrentRolledNumber()}")
            } else {
                println("Dice: ${dicePosition+1}. New rolled number: ${currentDice.roll()}")
            }
            player.incrementThrow(currentDice.getCurrentRolledNumber())
        }
        if(player.checkIsJamb()) {
            println("\n------JAMB!! Congrats------\n")
        }
        if(player.checkIsPoker()){
            println("\n------POKER! Congrats------\n")
        }
        if (player.checkIsScale()){
            println("\n------SCALE! Congrats------\n")
        }

        player.resetThrows()
        if(roundNumber!=3){
            println("Do you want to lock any dices? (y/n)")
            read = readLine()
            if(read=="y"){
                println("Enter the dice #'s one after another and type 'x' when you're done")
                read = readLine()
                while(read != "x"){
                    try {
                        read?.toInt()?.let {
                            player.getDice(it-1).disable()
                            numLocked++
                        }?: println("You didn't enter any dice number nor x to stop, \n" +
                                "Enter the dice #'s one after another and type 'x' when you're done")
                    }catch (ex: Exception){
                        println("You didn't enter any dice number nor x to stop, \n" +
                                "Enter the dice #'s one after another and type 'x' when you're done")
                    }

                    read=readLine()
                }
            }
            if (numLocked==6){               //if all six dices are locked the player can't play this round no more
                player.enableAll()
                return
            }
        }
    }
    player.enableAll()
}