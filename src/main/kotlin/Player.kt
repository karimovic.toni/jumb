
class Player(
    private var dices: MutableList<Dice> = mutableListOf(),
    private var throwCounters: MutableList<Int> = mutableListOf()
){

    init {
        for (i in 0 until 6){
            dices.add(Dice())
            throwCounters.add(0)
        }
    }

    /**
     *  Get one dice from list
     *
     *  @return Dice
     */
    fun getDice(position: Int): Dice{
        return dices[position]
    }

    /**
     *  Get one throw from list
     *
     *  @return Dice
     */
    private fun getThrow(position: Int): Int{
        return throwCounters[position]
    }

    /**
     *  Get number of dices that player have for game
     *
     *  @return Int
     */
    fun getNumberOfDicesAndThrows(): Int {
        return dices.size
    }

    /**
     *  Increment rolled dices number that user got after roll
     */
    fun incrementThrow(position: Int) {
        throwCounters[position-1]++
    }

    /**
     *  Reset rolled dices number that user got after roll
     */
    fun resetThrowCounter(position: Int) {
        throwCounters[position] = 0
    }

    /**
     *  Check did user rolled scale. Scale is when every number on dice repeats only once
     *
     *  @return Boolean
     */
    fun checkIsScale(): Boolean {
        for(count in 0 until getNumberOfDicesAndThrows()){
            if(count != 1){
                return false
            }else {
                continue
            }
        }
        return true
    }

    /**
     *  Check did user rolled Jamb. Jamb is when user rolls the same number on every dice
     *
     *  @return Boolean
     */
    fun checkIsJamb(): Boolean{
        for (throwPosition in 0 until getNumberOfDicesAndThrows()) {
            if (getThrow(throwPosition) == 6){
                return true
            }else {
                continue
            }
        }
        return false
    }

    /**
     *  Check did user rolled poker. Poker is when user rolls same number on 5 devices
     *
     *  @return Boolean
     */
    fun checkIsPoker(): Boolean{
        for (throwPosition in 0 until getNumberOfDicesAndThrows()) {
            if (getThrow(throwPosition) == 5){
                return true
            }else {
                continue
            }
        }
        return false
    }

    /**
     *  Reset all rolled dices number that user got after roll
     */
    fun resetThrows(){
        for (throwPosition in 0 until getNumberOfDicesAndThrows()) {
            resetThrowCounter(throwPosition)
        }
    }

    /**
     *  Enable all dices for throwing
     */
    fun enableAll() {
        for(dicePosition in 0 until getNumberOfDicesAndThrows()){
            dices[dicePosition].enable()
        }
    }
}