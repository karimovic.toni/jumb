

class Dice(
    private var number: Int = 0,
    private var isDisabled: Boolean = false
){

    /**
     *  Roll the dice to get a random number between 1 and 6, also save the number for future
     *  reference.
     *
     *  @return rolled number (random between 1 and 6)
     */
    fun roll(): Int{
        if(!isDisabled)
            number = (1..6).random()
        return number
    }

    /**
     *  Disable the dice so it can't be rolled
     */
    fun disable(){
        isDisabled = true
    }

    /**
     *  Enable the dice so it can be rolled
     */
    fun enable(){
        isDisabled = false
    }

    /**
     *  Get number that was rolled last
     *
     *  @return Int: last rolled number
     */
    fun getCurrentRolledNumber(): Int{
        return number
    }

    /**
     *  Get is dice disabled
     *
     *  @return Boolean: Is disabled
     */
    fun isDisabled(): Boolean{
        return isDisabled     //check if the dice is locked
    }
}
